export class Product {

    id: number;
    name: string;
    provider: string;
    ingredients: string[];
    description: string;
    age: number;
    cdtConservation: string;
    price: number;

    constructor(pname: string, pprovider: string, pingredients: string[], pdescription: string, page: number, pcdtConservation: string, pprice: number) {
        this.name = pname;
        this.provider = pprovider;
        this.ingredients = pingredients;
        this.description = pdescription;
        this.age = page;
        this.cdtConservation = pcdtConservation;
        this.price = pprice;

    }

}