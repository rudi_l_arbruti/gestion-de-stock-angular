import { Component } from '@angular/core';
import { Product } from './models/product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'StockManagment';

  // La composition des produits
  ingredientsMB = ["I5", "6Go RAM", "SSD 128Go"];
  ingredientsMBP = ["I7", "8Go RAM", "SSD 256Go"];
  ingredientsMBA = ["I3", "4Go RAM", "SSD 128Go"];
  ingredientsIMAC = ["I5", "8Go RAM", "SSD 512Go"];
  ingredientsMACP = ["I7", "16Go RAM", "SSD 1To"];


  tabProducts = [
    new Product("MacBook", "Apple", this.ingredientsMB, "Retina 13 pouces", 2015, "Utilisation entre -10°C et 38°C ", 1350),
    new Product("MacBook Pro", "Apple", this.ingredientsMBP, "Retina 15 pouces", 2016, "Utilisation entre -10°C et 38°C ", 1850),
    new Product("MacBook Air", "Apple", this.ingredientsMBA, "Retina 11 pouces", 2017, "Utilisation entre -10°C et 38°C ", 1150),
    new Product("IMac", "Apple", this.ingredientsIMAC, "Retina 27 pouces", 2017, "Utilisation entre -10°C et 38°C ", 2250),
    new Product("MacPro", "Apple", this.ingredientsMACP, "Retina 27 pouces", 2017, "Utilisation entre -10°C et 38°C ", 3250),

  ];
  
}
